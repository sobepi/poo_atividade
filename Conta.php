<?php 
abstract class Conta{
    private $Nome;
    private $Login; 
    private $Saldo;
    private $tipoConta;
    
    public function __construct($Nome, $Login, $Saldo, $tipoConta){
        self::setNome($Nome);
        self::setLogin($Login);
        self::setSaldo($Saldo);
        self::setTipoConta($tipoConta);
        
    }
    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->Nome;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->Login;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->Saldo;
    }

    /**
     * @return mixed
     */
    public function getTipoConta()
    {
        return $this->tipoConta;
    }

    /**
     * @param mixed $Nome
     */
    public function setNome($Nome)
    {
        $this->Nome = $Nome;
    }

    /**
     * @param mixed $Login
     */
    public function setLogin($Login)
    {
        $this->Login = $Login;
    }

    /**
     * @param mixed $Saldo
     */
    public function setSaldo($Saldo)
    {
        $this->Saldo = $Saldo;
    }

    /**
     * @param mixed $tipoConta
     */
    public function setTipoConta($tipoConta)
    {
        $this->tipoConta = $tipoConta;
    }

   }


?>
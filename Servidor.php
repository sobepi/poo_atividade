<?php 
 class Servidor{
    private  $codigo;
    private  $cargo;
    
    public function __construct()
    /**
     * @return mixed
     */
    public  function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @return mixed
     */
    public  function getCargo()
    {
        return $this->cargo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @param mixed $cargo
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;
    }
    
}
    
    



?>